﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
using System;

public abstract class CustomAssetImporter <T> : AssetImporter, ICustomAssetImporter
	where T : ScriptableObject
{	
	public void ImportAsset (string path, ScriptableObject asset)
	{
		ImportAsset(path, (T)asset);
	}
	public abstract void ImportAsset (string path, T asset);
	
	public Type GetAssetType ()
	{
		return typeof(T);
	}
}

public interface ICustomAssetImporter
{
	void ImportAsset (string path, ScriptableObject asset);
	Type GetAssetType ();
}