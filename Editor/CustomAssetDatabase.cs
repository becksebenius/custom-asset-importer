﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;

[InitializeOnLoad]
public class CustomAssetDatabase : AssetPostprocessor
{
	class CustomAssetType
	{
		public string Extension;
		public string IconPath;
		public ICustomAssetImporter Importer;
		public Type AssetType;
	}
	static Dictionary<string, CustomAssetType> extensionDict = new Dictionary<string, CustomAssetType>();
	static Dictionary<Type, CustomAssetType> assetTypeDict = new Dictionary<Type, CustomAssetType>();
	
	static CustomAssetDatabase ()
	{
		var assemblies = AppDomain.CurrentDomain.GetAssemblies();
		foreach(var assembly in assemblies)
		{
			var types = assembly.GetTypes();
			foreach(var type in types)
			{
				
				if(!typeof(ICustomAssetImporter).IsAssignableFrom(type))
					continue;
				
				var attributes = type.GetCustomAttributes(typeof(CustomAssetTypeAttribute), true) as CustomAssetTypeAttribute[];
				if(attributes.Length == 0) continue;
				
				var attribute = attributes[0];
				
				var assetType = new CustomAssetType();
				assetType.Extension = attribute.Extension;
				assetType.IconPath = attribute.IconPath;
				assetType.Importer = Activator.CreateInstance(type) as ICustomAssetImporter;
				assetType.AssetType = assetType.Importer.GetAssetType();
				
				extensionDict.Add(assetType.Extension, assetType);
				assetTypeDict.Add(assetType.AssetType, assetType);
			}
		}
	}
	
	static void OnPostprocessAllAssets (
        string[] importedAssets,
        string[] deletedAssets,
        string[] movedAssets,
        string[] movedFromAssetPaths
	){
        foreach (var str in importedAssets)
		{
			var fileInfo = new FileInfo(str);
			CustomAssetType assetType;
			
			if(!extensionDict.TryGetValue(fileInfo.Extension, out assetType))
			{
				continue;
			}
			
			if(assetType.Importer == null)
			{
				continue;
			}
			
			var path = str.Substring(0, str.Length - fileInfo.Extension.Length) + ".asset";
			var asset = AssetDatabase.LoadAssetAtPath(path, assetType.AssetType) as ScriptableObject;
			if(!asset)
			{
				asset = ScriptableObject.CreateInstance(assetType.AssetType);
				AssetDatabase.CreateAsset(asset, path);
			}
			assetType.Importer.ImportAsset (str, asset);
		}
    }
	
	public Type GetAssetType (string extension)
	{
		CustomAssetType type = null;
		if(!extensionDict.TryGetValue (extension, out type))
		{
			Debug.LogError("Custom Asset type with extension " + extension + " does not exist.");
			return null;
		}
		return type.AssetType;
	}
	
	public string GetExtension (Type type)
	{
		CustomAssetType assetType = null;
		if(!assetTypeDict.TryGetValue (type, out assetType))
		{
			Debug.LogError("Type " + type.Name + " does not have a custom asset importer.");
			return null;
		}
		return assetType.Extension;
	}
}
