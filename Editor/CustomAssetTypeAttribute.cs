using UnityEngine;
using System;

[AttributeUsage(AttributeTargets.Class)]
public class CustomAssetTypeAttribute : Attribute
{
	public string Extension { get; private set; }
	public string IconPath { get; private set; }
	
	public CustomAssetTypeAttribute (string extension, string iconPath)
	{
		this.Extension = extension;
		this.IconPath = iconPath;
	}
}